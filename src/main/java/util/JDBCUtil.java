package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {


    private  static  String url = "jdbc:mysql://localhost:3306/test?serverTimezone=Asia/Chongqing";
    private  static  String  driverName = "com.mysql.cj.jdbc.Driver";
    private  static  String  userName = "root";// root
    private  static  String  password = "521131";// 123456

    public static Connection getConnection() throws SQLException {
        Connection con = null;
        con = DriverManager.getConnection(url, userName, password);
        return con;
    }

    /**
     * 有时仅需要关闭Connection，就可以调用此方法
     * @param con
     */
    public static void closeConnection(Connection con) {
        if (con != null) {
            try {
                con.close();
                con = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static void realeaseAll(ResultSet rs, Statement st, Connection con) {
        if (rs != null) {
            try {
                rs.close();
                rs = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
                st = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        closeConnection(con);
    }
}

