package util;

import model.Student;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author 林洁颖
 */
public class POIUtil {
    /**
     * 存储状态
     * @param pathName
     * @param descOrderStudentList
     */
    public static boolean  exportExcel(String pathName, List<Student>  descOrderStudentList){
        File file = new File(pathName);
        /*
        当文件路径存在时，删除并重新创建。不存在时，直接重新创建
         */
        if(file.exists()){
            file.delete();
        }
        try {

            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file.exists()) {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("学生学习情况报表");
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("学号");
            row.createCell(1).setCellValue("姓名");
            row.createCell(2).setCellValue("班级");
            row.createCell(3).setCellValue("数学成绩");
            row.createCell(4).setCellValue("数学班级平均成绩");
            row.createCell(5).setCellValue("java成绩");
            row.createCell(6).setCellValue("java班级平均成绩");
            row.createCell(7).setCellValue("体育成绩");
            row.createCell(8).setCellValue("体育班级平均成绩");
            row.createCell(9).setCellValue("总成绩");
            row.createCell(10).setCellValue("班级平均总成绩");
            for(int i=1;i<=descOrderStudentList.size();i++){
                if(descOrderStudentList.get(i-1)!=null){
                    row=sheet.createRow(i);
                    XSSFCell cell;
                    cell=row.createCell(0, CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getStudentId());
                    cell=row.createCell(1,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getName());
                    cell=row.createCell(2,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getClassName());
                    cell=row.createCell(3,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getMathScore());
                    cell=row.createCell(4,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getMathScoreAvg());
                    cell=row.createCell(5,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getJavaScore());
                    cell=row.createCell(6,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getJavaScoreAvg());
                    cell=row.createCell(7,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getPEScore());
                    cell=row.createCell(8,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getPEScoreAvg());
                    cell=row.createCell(9,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getTotalScore());
                    cell=row.createCell(10,CellType.STRING);
                    cell.setCellValue(descOrderStudentList.get(i-1).getTotalScoreAvg());
                }
            }

            try ( FileOutputStream out=new FileOutputStream(pathName)){
                workbook.write(out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        return false;



    }

}
