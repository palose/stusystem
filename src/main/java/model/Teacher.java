package model;

public class Teacher {
    /**
     * 老师工号
     */
    private  String techerId;
    /**
     * 老师名字
     */
    private  String teacherName;
    /**
     * 老师的密码
     */
    private String teacherPwd="123456";

    public String getTecherId() {
        return techerId;
    }

    public void setTecherId(String techerId) {
        this.techerId = techerId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherPwd() {
        return teacherPwd;
    }

    public void setTeacherPwd(String teacherPwd) {
        this.teacherPwd = teacherPwd;
    }


    public Teacher(String techerId, String teacherName) {
        this.techerId = techerId;
        this.teacherName = teacherName;
    }
}
