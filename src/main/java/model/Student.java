package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 性别的集合
 */

public class Student {
    /**
     * 学号：学号自动生成，学号必须唯一
     */
    private String studentId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别
     */
    private String  genda=null;
    /**
     * 出生年月日
     */
    private Date birthday=null;
    /**
     * 学生班级名称
     */
    private  String className;
    private String studentPwd="123456";
    private String mathScore;
    private String javaScore;
    private String PEScore;
    private String totalScore;
    private String mathScoreAvg;
    private String javaScoreAvg;
    private String PEScoreAvg;
    private String totalScoreAvg;


    //private List<Course> couseList=new ArrayList<>();


    public String getMathScoreAvg() {
        return mathScoreAvg;
    }

    public void setMathScoreAvg(String mathScoreAvg) {
        this.mathScoreAvg = mathScoreAvg;
    }

    public String getTotalScoreAvg() {
        return totalScoreAvg;
    }

    public void setTotalScoreAvg(String totalScoreAvg) {
        this.totalScoreAvg = totalScoreAvg;
    }

    public String getJavaScoreAvg() {
        return javaScoreAvg;
    }

    public void setJavaScoreAvg(String javaScoreAvg) {
        this.javaScoreAvg = javaScoreAvg;
    }

    public String getPEScoreAvg() {
        return PEScoreAvg;
    }

    public void setPEScoreAvg(String PEScoreAvg) {
        this.PEScoreAvg = PEScoreAvg;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenda() {
        return genda;
    }

    public void setGenda(String genda) {
        this.genda = genda;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPEScore() {
        return PEScore;
    }

    public void setPEScore(String PEScore) {
        this.PEScore = PEScore;
    }

    public String getMathScore() {
        return mathScore;
    }

    public void setMathScore(String mathScore) {
        this.mathScore = mathScore;
    }

    public String getJavaScore() {
        return javaScore;
    }

    public void setJavaScore(String javaScore) {
        this.javaScore = javaScore;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


    public String getStudentPwd() {
        return studentPwd;
    }

    public void setStudentPwd(String studentPwd) {
        this.studentPwd = studentPwd;
    }

    public Student(String studentId, String name, String genda, Date birthday) {
        this.studentId = studentId;
        this.name = name;
        this.genda = genda;
        this.birthday = birthday;
    }

    public Student(String studentId, String name, String genda, Date birthday, String className) {
        this.studentId = studentId;
        this.name = name;
        this.genda = genda;
        this.birthday = birthday;
        this.className = className;
    }

    public Student(String studentId, String name, String studentPwd, String mathScore, String javaScore, String PEScore) {
        this.studentId = studentId;
        this.name = name;
        this.studentPwd = studentPwd;
        this.mathScore = mathScore;
        this.javaScore = javaScore;
        this.PEScore = PEScore;
    }
    public Student(String studentId, String name, String mathScore, String javaScore, String PEScore) {
        this.studentId = studentId;
        this.name = name;
        this.studentPwd = studentPwd;
        this.mathScore = mathScore;
        this.javaScore = javaScore;
        this.PEScore = PEScore;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId='" + studentId + '\'' +
                ", name='" + name + '\'' +
                ", mathScore='" + mathScore + '\'' +
                ", javaScore='" + javaScore + '\'' +
                ", PEScore='" + PEScore + '\'' +
                '}';
    }

    /**
     * 学生登录的初始密码
     */





    public Student() {
        this.studentPwd="123456";
    }

}