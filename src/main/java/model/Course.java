package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 课程类，属性如下：课程号、课程名、负责的老师编号
 */
public class Course {
    /**
     * 课程号
     */
    private String courseId;
    /**
     * 课程名
     */
    private  String courseName;
    /**
     * 负责的老师编号
     */
   private String teacherId;

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }
}
