package dao;

public interface SystemDao {
    /**
     * 判断某个人是否有登录权限,flag为是学生还是老师
     * @param loginCode
     * @param pwd
     * @param flag
     * @return
     */
    public boolean loginAuthentication(String loginCode,String pwd,int flag);

    /**
     * 修改登录系统的密码,flag为是学生还是老师
     * @param loginCode
     * @param oldPwd
     * @param newPwd
     * @param flag
     * @return
     */
    public boolean changePwd(String loginCode,String oldPwd,String newPwd,int flag);



}
