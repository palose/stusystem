package dao;

import model.Course;

public interface CourseDao {
    /**
     * 添加课程
     * @param course
     * @return
     */
    public boolean addCourse(Course course);
}

