package dao.impl;

import dao.CourseDao;
import model.Course;
import model.Teacher;
import util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class CourseDaoImpl implements CourseDao {
    @Override
    public boolean addCourse(Course course) {
            Connection conn = null;
            PreparedStatement pstmt =null;
            String sql="insert into test.course (course_name,teacher_id,course_id) values(?,?,?)";
            int updateRow=0;

            try {
                conn= JDBCUtil.getConnection();
                pstmt= conn.prepareStatement(sql);
                pstmt.setString(1, course.getCourseName());
                pstmt.setString(2, course.getTeacherId());
                pstmt.setString(3, course.getCourseId());
                updateRow=pstmt.executeUpdate();
                if(updateRow>0){
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            }


            return false;
        }
}
