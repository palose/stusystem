package dao.impl;

import dao.SystemDao;
import util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author 林洁颖
 */
public class SystemDaoImpl implements SystemDao {
    /**
     * 判断某个人是否有登录权限,flag为是学生还是老师
     * @param loginCode
     * @param pwd
     * @param flag
     * @return
     */
    @Override
    public boolean loginAuthentication(String  loginCode, String pwd, int flag) {
        Connection conn = null;
        PreparedStatement pstmt =null;
        ResultSet rs=null;

        if(flag==0){
            try {
                String sql="select teacher_pwd from test.teacher where teacher_id= ?";
                conn=JDBCUtil.getConnection();
                pstmt= conn.prepareStatement(sql);
                pstmt.setString(1,loginCode);
                rs= pstmt.executeQuery();
                if(rs==null){
                    return false;
                }
                while(rs.next()){
                    String corPwd=rs.getString("teacher_pwd");
                    return corPwd.equals(pwd);
                    /**
                     * 密码错误
                     */
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                JDBCUtil.realeaseAll(rs,pstmt,conn);

            }


        }
        /**
         * 是学生，对学生数据库里的表进行操作
         */
        if(flag==1){
            try {
                String sql="select stu_pwd from test.student where stu_id= ?";
                conn=JDBCUtil.getConnection();
                pstmt= conn.prepareStatement(sql);
                pstmt.setString(1,loginCode);
                rs= pstmt.executeQuery();
                /**
                 * 用户名不存在
                 */
                if(rs==null){
                    return false;
                }
                while(rs.next()){
                    String corPwd=rs.getString("stu_pwd");
                    if(corPwd.equals(pwd)){
                        return true;
                    }
                    /**
                     * 密码错误
                     */
                    return false;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                JDBCUtil.realeaseAll(rs,pstmt,conn);

            }



        }



        return false;
    }
    /**
     * 修改登录系统的密码,flag为是学生还是老师
     * @param loginCode
     * @param oldPwd
     * @param newPwd
     * @param flag
     * @return
     */

    @Override
    public boolean changePwd(String loginCode, String oldPwd, String newPwd, int flag) {
        Connection conn = null;
        PreparedStatement pstmt =null;
        int updateRow=0;
        if(loginAuthentication(loginCode,oldPwd,flag)){
            /**
             * 更新老师的登录密码
             */
           if(flag==0){
               try {
                   String sql="update test.teacher set teacher_pwd= ? where teacher_id =?";
                   conn=JDBCUtil.getConnection();
                   pstmt= conn.prepareStatement(sql);
                   pstmt.setString(1,newPwd);
                   pstmt.setString(2,loginCode);
                   updateRow=pstmt.executeUpdate();
                   if(updateRow>0){
                       return true;
                   }

               } catch (SQLException throwables) {
                   throwables.printStackTrace();
               } finally {
                   JDBCUtil.closeConnection(conn);
               }

           }
            /**
             * 更新学生登录密码
             */
            try {
                String sql="update test.student set stu_pwd= ?where stu_id =?";
                conn=JDBCUtil.getConnection();
                pstmt= conn.prepareStatement(sql);
                pstmt.setString(1,newPwd);
                pstmt.setString(2,loginCode);
                updateRow=pstmt.executeUpdate();
                if(updateRow>0) {
                    return true;
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } finally {
                JDBCUtil.closeConnection(conn);
            }


        }



        return false;
    }
    /*

    public static void main(String[] args) {
        SystemDaoImpl systemDao=new SystemDaoImpl();
        if(!  systemDao.loginAuthentication("201921000000","123456",1)){
            System.out.println("登录失败");
        }
        if(!  systemDao.changePwd("201921000000","123456","1234",1)){
            System.out.println("修改密码失败");
        }


    }
    */



}
