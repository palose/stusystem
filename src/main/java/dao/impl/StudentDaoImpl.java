package dao.impl;

import dao.StudentDao;
import model.Student;
import util.JDBCUtil;

import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    @Override
    public void addStudent(Student student) {
        Connection conn = null;
        Statement stat = null;
        PreparedStatement pstmt =null;
        //在数据库中插入学生姓名、学号、性别、出生年月日
        if(searchStuByStuId(student.getStudentId(),0)!=null){
            return;
        }
        String sql = "insert into test.student (stu_id,stu_name,stu_genda,stu_birthday,class_name,stu_pwd) values(?,?,?,?,?,?)";

        try {
            conn = JDBCUtil.getConnection();
            stat = conn.createStatement();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,student.getStudentId());
            pstmt.setString(2, student.getName());
            pstmt.setString(3,student.getGenda());
            pstmt.setDate(4, new java.sql.Date( new java.util.Date(String.valueOf(student.getBirthday())).getTime()));
            pstmt.setString(5,student.getClassName());
            pstmt.setString(6,student.getStudentPwd());

            int updateRow= pstmt.executeUpdate();
            //获得受sql语句影响的行数
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.closeConnection(conn);

        }

    }

    @Override
    public Student searchStuByStuId(String studentId,int flag) {
        Connection conn = null;
        Statement stat = null;
        PreparedStatement pstmt =null;
        ResultSet rs=null;
        Student student=null;
        String sql="select * from test.student where stu_id = ?";
        try {
            conn = JDBCUtil.getConnection();
            stat = conn.createStatement();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,studentId);
            rs= pstmt.executeQuery();
            //使用gui后此句不用
            if(flag==0){
                if(rs.next()){
                    student=new Student(studentId,rs.getString("stu_name"),rs.getString("math_score"),rs.getString("java_score"),rs.getString("PE_score"));

                }
            }
            else{
                if(rs.next()){
                    java.util.Date date=new java.util.Date(rs.getDate("stu_birthday").getTime());
                    student=new Student(studentId,rs.getString("stu_name"),rs.getString("stu_genda"),date,rs.getString("class_name"),rs.getString("math_score"),rs.getString("java_score"),rs.getString("PE_score"));
                }


            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            JDBCUtil.realeaseAll(rs,stat,conn);
        }


        return student;
    }


    @Override
    public boolean addStuScore(String studentId, String courseName, String score) {
        Connection conn = null;
        Statement stat = null;
        PreparedStatement pstmt =null;
        String sql="update  test.student set "+courseName+"_score = ? where stu_id = ?";
        int updateRow=0;
        try {
            conn = JDBCUtil.getConnection();
            pstmt= conn.prepareStatement(sql);
            pstmt.setString(1,score);
            pstmt.setString(2,studentId);
            updateRow=pstmt.executeUpdate();
            if(updateRow>0){
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.closeConnection(conn);
        }

        return false;
    }


    @Override
    public List<Student> searchStuByStuName(String studentName) {
        List<Student> studentList=new ArrayList<>();
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement pstmt =null;
        //模糊查找语句,like，%是可能还有多个字符
        String sql = "SELECT * From test.student where stu_name like ?";
        try {
            conn = JDBCUtil.getConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1,"%"+studentName+"%");
            rs = pstmt.executeQuery();
            //之后在gui页面中出现提示

            while (rs.next()) {
                Student student=new Student();
                student=new Student(rs.getString("stu_id"),rs.getString("stu_name"),rs.getString("math_score"),rs.getString("java_score"),rs.getString("PE_score"));
                studentList.add(student);
            }

        } catch (

                SQLException sqle) {
            sqle.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            JDBCUtil.realeaseAll(rs, pstmt, conn);
        }

        return studentList;

    }

    @Override
    public List<Student> generateClassStuReport(String className,String filePath) {
        Connection conn = null;
        PreparedStatement pstmt =null;
        ResultSet rs = null;
        List<Student> descOrderStudentList=new ArrayList<>();
        double mathScoreAvg=0.0;
        double javaScoreAvg=0.0;
        double PEScoreAvg=0.0;
        double totalScoreAvg=0.0;
        DecimalFormat df   =new   DecimalFormat("#.00");

        String sql="select * from test.student where class_name = ?";

        try {
            conn = JDBCUtil.getConnection();
            pstmt= conn.prepareStatement(sql);
            pstmt.setString(1,className);
            rs= pstmt.executeQuery();
            while(rs.next()){
                 String mathScore=rs.getString("math_score");
                 String javaScore=rs.getString("java_score");
                 String PEScore=rs.getString("PE_score");
              Student  student=new Student(rs.getString("stu_id"),rs.getString("stu_name"),mathScore,javaScore,PEScore);
             student.setClassName(className);
              mathScoreAvg+=Double.valueOf(mathScore);
              javaScoreAvg+=Double.valueOf(javaScore);
              PEScoreAvg+=Double.valueOf(PEScore);
              totalScoreAvg+=Double.valueOf(mathScore)+Double.valueOf(javaScore)+Double.valueOf(PEScore);

              student.setTotalScore(df.format(Double.valueOf(mathScore)+Double.valueOf(javaScore)+Double.valueOf(PEScore)));
              //有了gui之后就不要了
              descOrderStudentList.add(student);
            }
            mathScoreAvg/=descOrderStudentList.size();
            javaScoreAvg/=descOrderStudentList.size();
            PEScoreAvg/=descOrderStudentList.size();
            totalScoreAvg/=descOrderStudentList.size();
            for (Student stu:
                 descOrderStudentList) {
                stu.setMathScoreAvg(df.format(mathScoreAvg));
                stu.setJavaScoreAvg(df.format(javaScoreAvg));
                stu.setPeScoreAvg(df.format(PEScoreAvg));
                stu.setTotalScoreAvg(df.format(totalScoreAvg));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
         Collections.sort(descOrderStudentList, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o2.getTotalScore().compareTo(o1.getTotalScore());
            }
        });
        return descOrderStudentList;
    }

    @Override
    public boolean reviseStuInfo(Student student) {
        Connection conn = null;
        PreparedStatement pstmt =null;
        String sql="update test.student set stu_name = ? , stu_genda = ?, stu_birthday = ? ,class_name = ? ,math_score =? ,java_score =? ,PE_score =? where stu_id =?";
        int updateRow=0;
        try {
            conn = JDBCUtil.getConnection();
            pstmt=conn.prepareStatement(sql);
            pstmt.setString(1,student.getName());
            pstmt.setString(2,student.getGenda());
            pstmt.setDate(3, new java.sql.Date(student.getBirthday().getTime()));
            pstmt.setString(4,student.getClassName());
            pstmt.setString(5,student.getMathScore());
            pstmt.setString(6,student.getJavaScore());
            pstmt.setString(7,student.getPeScore());
            pstmt.setString(8,student.getStudentId());
            //获得受sql语句影响的行数
            updateRow=pstmt.executeUpdate();
            return  updateRow>0;

        } catch  (SQLException    e)  {
            e.printStackTrace();
        }
        finally {
            JDBCUtil.closeConnection(conn);
        }

        return false;
    }

    @Override
    public boolean deleteStudent(String studentId) {
        Connection conn = null;
        PreparedStatement pstmt =null;
        ResultSet rs = null;

        String sql="delete From test.student where stu_id = ?";
        try {
            conn = JDBCUtil.getConnection();
             pstmt= conn.prepareStatement(sql);
             pstmt.setString(1,studentId);
            int updateRow= pstmt.executeUpdate();
            //获得受sql语句影响的行数

            return updateRow>0;
        } catch  (SQLException    e)  {
            e.printStackTrace();
        }
        finally {
            JDBCUtil.closeConnection(conn);
        }

        return false;
    }

    @Override
    public List<String> getStuIdList(String courseName) {
        List<String> stuIdList=new ArrayList<>();
        Connection conn = null;
        Statement stat = null;

        ResultSet rs = null;
        String sql="select stu_id from test.student where "+courseName+"_score is null or trim("+courseName+"_score)=''";
        try {
            conn = JDBCUtil.getConnection();
            stat= conn.createStatement();
            rs= stat.executeQuery(sql);
            while(rs.next()){
                String str=rs.getString("stu_id");
                stuIdList.add(str);
            }
            //获得受sql语句影响的行数

        } catch  (SQLException    e)  {
            e.printStackTrace();
        }
        finally {
            JDBCUtil.realeaseAll(rs,stat,conn);
        }


        return stuIdList;
    }

}
