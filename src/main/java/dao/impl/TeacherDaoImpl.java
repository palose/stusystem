package dao.impl;

import dao.TeacherDao;
import model.Teacher;
import util.JDBCUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class TeacherDaoImpl implements TeacherDao {
    @Override
    public boolean addTeacher(Teacher teacher) {
        Connection conn = null;
        PreparedStatement pstmt =null;
        String sql="insert into test.teacher (teacher_id,teacher_name,teacher_pwd) values(?,?,?)";
        int updateRow=0;

        try {
            conn= JDBCUtil.getConnection();
            pstmt= conn.prepareStatement(sql);
            pstmt.setString(1, teacher.getTecherId());
            pstmt.setString(2,teacher.getTeacherName());
            pstmt.setString(3,teacher.getTeacherPwd());
            updateRow=pstmt.executeUpdate();
            if(updateRow>0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }


        return false;
    }
}
