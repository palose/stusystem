package dao;

import model.Teacher;

/**
 * @author 林洁颖
 */
public interface TeacherDao {
    public boolean addTeacher(Teacher teacher);

}
