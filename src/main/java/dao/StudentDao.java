package dao;

import model.Student;

import java.util.List;

/**
 * @author 林洁颖
 */
public interface StudentDao {


    /**
     * 添加学生功能：姓名、学号、性别、出生年月日
     * @param student
     */
    public default void addStudent(Student student) {
    }

    /**
     * 通过学号查找学生
     * @param studentId
     * @param flag
     * @return
     */
    public  Student searchStuByStuId(String studentId,int flag);

    /**
     * 给学生根据课程名输入成绩
     * @param studentId
     * @param courseName
     * @param score
     * @return
     */
    public boolean addStuScore(String studentId,String courseName,String score);

    /**
     * 通过学生姓名查找学生
     * @param studentName
     * @return
     */
    public List<Student> searchStuByStuName(String studentName);

    /**
     * 生成班级学生成绩情况报表
     * @param classId
     * @param filePath
     * @return
     */
    public List<Student> generateClassStuReport(String classId,String filePath);

    /**
     * 对除学号外的学生信息进行修改
     * @param student
     * @return
     */
    public boolean reviseStuInfo(Student student);

    /**
     * 对学生信息进行删除
     * @param studentId
     * @return
     */
    public boolean deleteStudent(String studentId);

    /**
     * 获得待输入某科成绩的学生学号列表
     * @param courseName
     * @return
     */
    public List<String >getStuIdList(String courseName);






}
