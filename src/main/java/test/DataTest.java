package test;

import util.RandomStudentUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataTest {
    public static boolean generateTestData(String testFilePathName){
        File file=new File(testFilePathName);
        try {
            if(file.exists()){
                file.delete();
            }
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        try (FileWriter fw = new FileWriter(file);){

            for (int i = 0; i < 100000; i++) {
                String name = RandomStudentUtil.getChineseName();
                String id=RandomStudentUtil.getId();
                StringBuilder studentData =new StringBuilder();
                studentData.append(id).append(" ").append(name).append(" ").append(RandomStudentUtil.getRadomScore()).append(" ").append(RandomStudentUtil.getRadomScore()).append(" ").append(RandomStudentUtil.getRadomScore());
                studentData.append("\r\n");
                fw.write(String.valueOf(studentData));

            }
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }



}
